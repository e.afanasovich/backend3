<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
<div>
  <form action="sign_up.php" method="POST">

    <label>Введите имя: <br>
      <!-- нужно экранирование  -->
      <input type="text" placeholder="testname" name="login">
    </label>
    <br>
    <br>
    <label>Введите eMail: <br>
      <input type="email" placeholder="test@example.com" name="email">
    </label>
    <br>
    <br>
    <label>Введите год рождения: <br>
      <!-- select-раскрывающийся список с  вариантами option  -->
      <select id="" name="date">
        <option value="1999" >1999</option>
        <option value="2000" >2000</option>
        <option value="2001" >2001</option>
        <option value="2002" >2002</option>
      </select>
    </label>
    <br>
    <br>
    <label>Выберите пол: <br>
      <input type="radio" name="sex" value="male">Муж
      <input type="radio" name="sex" value="female">Жен
    </label>
    <br>
    <br>
    <label>Выберите количество конечностей <br>
      <input type="radio" name="amount" value="3">3
      <input type="radio" name="amount" value="4">4
      <input type="radio" name="amount" value="2,5">2,5
    </label>
    <br>
    <br>
    <label>Выберите сверхспособность:<br>
      <select multiple="multiple" name="superpowers">
        <!-- size="value"-задает размер списка в ручную,multiple-задает размер списка автоматически -->
        <option value="Бессмертие">Бессмертие</option>
        <option value="Прохождение сковзь стены">Прохождение сковзь стены</option>
        <option value="Левитация">Левитация</option>
      </select>
    </label>
    <br>
    <br>
    <label>Биография: <br>

      <textarea name="biography" id="" cols="20" rows="3"
                placeholder="Расскажите о себе"></textarea>
    </label>
    <br>
    <br>
    <label>С контрактом ознакомлен: <br>
      <input class="right" type="checkbox" name="contract" value="yes">
    </label>
    <br>
    <br>
    <input type="submit" name="do_signup" value="submit ">
  </form>
</div>
</body>
</html>
